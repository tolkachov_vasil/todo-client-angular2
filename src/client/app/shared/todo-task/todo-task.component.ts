import { Component, Input, OnInit, forwardRef } from '@angular/core';

import { DataService } from '../services/data.service';

import {
    TodoTagComponent,
    AutoFocusDirective,
    ProgressBarComponent,
    ModalWindowComponent,
} from '../index';

import { ITag, ITask, IFriend } from '../models/index';
import { TaskSortPipe, TagSortPipe } from '../pipes/index';


@Component({
    moduleId: module.id,
    selector: 'todo-task',
    templateUrl: './todo-task.component.html',
    styleUrls: ['./todo-task.component.css'],
    directives: [
        forwardRef(() => ProgressBarComponent),
        forwardRef(() => ModalWindowComponent),
        TodoTagComponent,
        TodoTaskComponent,
        AutoFocusDirective,
    ],
    pipes: [TaskSortPipe, TagSortPipe]
})
export class TodoTaskComponent implements OnInit {
    @Input() task: ITask;
    private isAllowEditorOk: boolean;
    private isEditMode: boolean;

    constructor(
        private dataService: DataService) {
    }

    ngOnInit() {
        this.isEditMode = this.task._isNewTaskForm;
    }

    getAllTags() {
        return this.dataService.getAllTags();
    }

    getFriends() {
        return this.dataService.getFriends();
    }

    getFriendTags(friend: IFriend): ITag[] {
        return this.dataService.getFriendTags(friend);
    }

    assignFriend(friend: IFriend): void {
        this.task._islocked = true;
        this.dataService.assignFriend(this.task, friend);
    }

    getAssignedFriend(): IFriend {
        return this.dataService.getAssignedFriend(this.task);
    }

    checkTitleInput(text: string) {
        this.isAllowEditorOk = text.length > 0;
    }

    toggleFold() {
        this.task.unfold = !this.task.unfold;
    }

    toggleStar() {
        this.dataService.toggleTaskStar(this.task);
    }

    getProgress(): number {
        return this.dataService.getTaskProgress(this.task);
    }

    getTaskTags(): ITag[] {
        return this.dataService.getTagsByIds(this.task.tags);
    }

    getChildTasks(): ITask[] {
        return this.dataService.getChildTasks(this.task);
    }

    doEdit(): void {
        if (!this.task._islocked) {
            this.isEditMode = true;
            this.isAllowEditorOk = true;
        }
    }

    updateTitle(text: string): void {
        text = text.trim();
        if (text.length < 1) return;
        if (this.task._isNewTaskForm) {
            this.dataService.addTask(text, this.task.tags);
            this.task.title = '';
            this.task.tags = [];
            this.isAllowEditorOk = false;
        } else {
            console.log('ok: ', text);
            this.task.title = text;
            this.isEditMode = false;
            this.doSync();
        }
    }

    doSync(): void {
        this.dataService.syncTask(this.task);
    }

    doCheck(): void {
        this.dataService.toggleTaskCheck(this.task);
    }

    doDelete(): void {
        this.dataService.deleteTaskTree(this.task);
    }

    unselectTag(tag: ITag): void {
        this.dataService.removeTaskTag(this.task, tag);
    }

    selectTag(tag: ITag): void {
        this.dataService.addTaskTag(this.task, tag);
    }

    doSplit(): void {
        this.dataService.splitTask(this.task);
    }

    isHideByTag(): boolean {
        let isHide: boolean = this.task.tags.length > 0;
        let tags: ITag[] = this.dataService.getTaskTags(this.task);
        tags.forEach(tag => {
            if (!tag.hide) isHide = false;
        });
        return isHide;
    }


}


