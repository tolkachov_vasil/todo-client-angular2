import { Directive, ElementRef } from '@angular/core';

@Directive({ selector: '[autoFocus]' })
export class AutoFocusDirective {
    constructor(public el: ElementRef) {
        this.el.nativeElement.focus();
    }
}