import { Component, Input, OnInit, forwardRef } from '@angular/core';

import { IUser, ITag, Tag, IFriend } from '../models/index';
import { TagSortPipe } from '../pipes/index';
import { DataService } from '../services/data.service';
import { ModalWindowComponent, TodoTagComponent } from '../index';

@Component({
  moduleId: module.id,
  selector: 'todo-toolbar',
  templateUrl: 'todo-toolbar.component.html',
  styleUrls: ['./todo-toolbar.component.css'],
  directives: [
    forwardRef(() => ModalWindowComponent),
    TodoTagComponent,
  ],
  pipes: [TagSortPipe]
})
export class TodoToolbarComponent implements OnInit {
  @Input() user: IUser;
  private selectedTag: ITag;
  private selectedFriend: IFriend;
  private newTag: ITag;
  private isAddTagMode: boolean;
  private isShowTags: boolean;
  private isTagEdit: boolean;

  constructor(
    private dataService: DataService) { }

  ngOnInit() {
    this.newTag = new Tag();
    this.newTag.name = '';
    this.newTag.color = '#ffffff';
  }

  getAllTags(): ITag[] {
    return this.dataService.getAllTags();
  }

  getFriendTags(friend: IFriend): ITag[] {
    return this.dataService.getFriendTags(friend);
  }

  selectTag(tag: ITag) {
    if (this.isAddTagMode) {
      this.selectFriendTag(tag);
      this.isShowTags = false;
    } else {
      this.selectedTag = tag;
      this.isTagEdit = true;
      this.selectedTag = tag;
    }
  }

  selectFriend(friend: IFriend) {
    this.selectedFriend = friend;
  }

  unselectFriendTag(tag: ITag): void {
    this.dataService.removeFriendTag(this.selectedFriend, tag);
  }

  selectFriendTag(tag: ITag): void {
    this.dataService.addFriendTag(this.selectedFriend, tag);
    // this.isShowTagsList = false;
  }

  updateTag() {
    if (this.selectedTag === this.newTag) {
      console.log('this.selectedTag: ', this.selectedTag);
      this.dataService.createTag(this.selectedTag.name, this.selectedTag.color);
      this.newTag.name = '';
      this.newTag.color = '#ffffff';
    } else {
      this.dataService.syncTag(this.selectedTag);
    }
  }

  updateFriendTags() {
    this.dataService.syncFriend(this.selectedFriend);
  }

  deleteTag() {
    this.dataService.deleteTag(this.selectedTag);
  }

}
