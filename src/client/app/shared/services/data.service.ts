import { Injectable } from '@angular/core';

import { UserService } from '../services/user.service';
import { Tag, ITask, Task, ITag, IFriend } from '../models/index';

@Injectable()
export class DataService {
    private tags: ITag[];
    private tasks: ITask[];
    private friends: IFriend[];
    // private _progress: number;
    private _allChildsCount: number;
    private _checkedCount: number;
    // private _assignedPersonsImages: string[];
    private _starsCount: number;
    private _assignedPersonsCount: number;

    constructor(
        private userService: UserService) {

        console.log('construct DataService');
        this.tags = [];
        this.tasks = [];
        this.userService.tasks$.subscribe(tasks => {
            this.tasks = tasks;
            this.updateTags();
        });
        this.userService.tags$.subscribe(tags => {
            this.tags = tags;
            this.updateTags();
        });
        this.userService.friends$.subscribe(friends => {
            this.friends = friends;
        });
    }

    public getAllTags(): ITag[] {
        return this.tags;
    }

    public getTaskTags(task: ITask): ITag[] {
        let result: ITag[] = [];
        task.tags.forEach(id => result.push(this.getTagById(id)));
        return result;
    }

    public getFriendTags(user: IFriend): ITag[] {
        if (!user) return [];
        let result: ITag[] = [];
        user.tags.forEach(id => result.push(this.getTagById(id)));
        return result;
    }

    public getFriends(): IFriend[] {
        return this.friends;
    }

    public assignFriend(task: ITask, friend: IFriend): void {
        task.assign = friend._id;
        this.syncTask(task);
    }

    public getAssignedFriend(task: ITask): IFriend {
        return this.getFriendById(task.assign);
    }

    public getRootTasks(): ITask[] {
        let result: ITask[] = [];
        this.tasks.forEach((task: ITask) => {
            if (!task.inner) result.push(task);
        });
        return result;
    }

    public getChildTasks(parent: ITask) {
        let result: ITask[] = [];
        parent.child.forEach((id: number) => {
            this.tasks.forEach((task: ITask) => {
                if (task.id === id) result.push(task);
            });
        });
        return result;
    }

    public getTaskById(id: number): ITask {
        let result: ITask = null;
        this.tasks.forEach(task => {
            if (task.id === id) result = task;
        });
        return result;
    }

    public getFriendById(id: any): IFriend {
        let result: IFriend = null;
        this.friends.forEach(friend => {
            if (friend._id === id) result = friend;
        });
        return result;
    }

    public deleteTask(task: ITask) {
        this.getChildTasks(task).forEach((child: ITask) => this.deleteTask(child));
        this.tasks.splice(this.tasks.indexOf(task), 1);
    }

    public deleteTaskTree(task: ITask) {
        this.deleteTask(task);
        // this.updateTasks();
        this.updateTags();
    }

    public getTagById(id: number): ITag {
        let result: ITag = null;
        this.tags.forEach(tag => {
            if (tag.id === id) result = tag;
        });
        return result;
    }

    public getTagsByIds(ids: number[]): ITag[] {
        let result: ITag[] = [];
        ids.forEach((id: number) => result.push(this.getTagById(id)));
        return result;
    }

    public getTaskProgress(task: ITask): number {
        if (task.child.length < 1) return;
        this._allChildsCount = 0;
        this._checkedCount = 0;
        this.scanTask(task);
        let progress: number = 100 / this._allChildsCount * this._checkedCount;
        task.checked = progress > 99.99;
        return progress;
    }

    public toggleTaskCheck(task: ITask): void {
        task.checked = !task.checked;
        let parents = this.getAllParentTasks(task);
        parents.forEach(parent => this.getTaskProgress(parent));
        this.syncTask(task);
    }

    public toggleTaskStar(task: ITask): void {
        task.star = !task.star;
        this.syncTask(task);
    }

    public getAllParentTasks(child: ITask): ITask[] {
        let result: ITask[] = [];
        let task: ITask = null;
        while (true) {
            task = this.getParentTask(child);
            if (task === null) break;
            result.push(task);
            child = task;
        }
        return result;
    }

    public removeTaskTag(task: ITask, tag: ITag): void {
        let index: number = task.tags.indexOf(tag.id);
        if (index !== -1) {
            task.tags.splice(index, 1);
            tag.use--;
            this.syncTask(task);
            this.syncTag(tag);
        }
    }

    public addTaskTag(task: ITask, tag: ITag): void {
        task.tags.push(tag.id);
        tag.use++;
        this.syncTask(task);
        this.syncTag(tag);
    }

    public removeFriendTag(friend: IFriend, tag: ITag): void {
        let index: number = friend.tags.indexOf(tag.id);
        if (index !== -1) {
            friend.tags.splice(index, 1);
            this.syncFriend(friend);
        }
    }

    public addFriendTag(friend: IFriend, tag: ITag): void {
        friend.tags.push(tag.id);
        this.syncFriend(friend);
    }

    public getParentTask(child: ITask): ITask {
        let result: ITask = null;
        this.tasks.forEach(task => {
            if (task.child.indexOf(child.id) !== -1)
                result = task;
        });
        return result;
    }

    public getUsedTags(): ITag[] {
        let result: ITag[] = [];
        this.tags.forEach((tag: ITag) => {
            if (tag.use > 0) result.push(tag);
        });
        return result;
    }

    public updateTagUsesCount(): void {
        let usesCounter: number[] = [];
        this.tags.forEach((tag: ITag) => {
            usesCounter[tag.id] = 0;
            tag.use = 0;
        });
        this.getRootTasks().forEach((task: ITask) => {
            task.tags.forEach((tagId: number) => usesCounter[tagId] += 1);
        });
        this.tags.forEach((tag: ITag) => tag.use = usesCounter[tag.id]);
    }

    public createTag(name: string, color: string): void {
        let newTag = new Tag();
        newTag.name = name;
        newTag.color = color;
        newTag.id = this.getUnusedId(this.tags);
        this.tags.push(newTag);
    }


    public syncFriend(friend: IFriend): void {
        // todo server sync
    }

    public syncTask(task: ITask): void {
        // todo server sync
    }

    public syncTag(tag: ITag): void {
        // todo server sync
    }


    public syncAllTasks(): void {
        // todo server sync
    }

    public syncAllTags(): void {
        // todo server sync
    }

    public deleteTag(trashTag: ITag): void {
        let index: number;
        this.tasks.forEach(task => {
            index = task.tags.indexOf(trashTag.id);
            if (index !== -1) task.tags.splice(index, 1);
        });

        for (index = 0; index < this.tags.length; index++) {
            if (this.tags[index].id === trashTag.id)
                this.tags.splice(index, 1);
        }
        this.syncAllTasks();
        this.syncAllTags();
    }

    public splitTask(parentTask: ITask): void {
        parentTask.unfold = true;
        let childTask: ITask = this.addTask('', []);
        let newId: number = childTask.id;
        parentTask.child.push(newId);
        childTask.inner = true;
        childTask.title = parentTask.child.length + '. ';
        this.syncTask(parentTask);
        this.syncTask(childTask);
    }

    public addTask(title: string, tags: number[]): ITask {
        let newTask: ITask = new Task();
        newTask.child = [];
        newTask.title = title;
        newTask.tags = tags;
        newTask.id = this.getUnusedId(this.tasks);
        newTask.date = new Date();
        this.tasks[newTask.id] = newTask;
        console.log('tasks', this.tasks);
        this.syncTask(newTask);
        return newTask;
    }

    private getUnusedId(collection: Array<ITag | ITask>): number {
        let maxId: number = 0;
        collection.forEach((element: ITag | ITask) => {
            maxId = Math.max(maxId, element.id);
        });
        return maxId + 1;
    }

    private updateTags(): void {
        this.updateTagUsesCount();
    }

    private scanTask(task: ITask): void {
        this.getChildTasks(task).forEach((child: ITask) => {
            if (child.child.length > 0) {
                this.scanTask(child);
            } else {
                this._allChildsCount++;
                this._checkedCount += (child.checked ? 1 : 0);
                if (child.assign) {
                    this._assignedPersonsCount++;
                }
                if (child.star) {
                    this._starsCount++;
                }
            }
        });
    };


}
