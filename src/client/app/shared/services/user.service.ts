import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { IUser, ITask, ITag, IFriend } from '../models/index';

const USER_URL = 'api/user/';

@Injectable()
export class UserService {

  private _user$: Subject<IUser>;
  private _tasks$: Subject<ITask[]>;
  private _tags$: Subject<ITag[]>;
  private _friends$: Subject<IFriend[]>;

  get user$() { return this._user$.asObservable(); }
  get tasks$() { return this._tasks$.asObservable(); }
  get tags$() { return this._tags$.asObservable(); }
  get friends$() { return this._friends$.asObservable(); }


  constructor(private http: Http) {
    console.log('construct UserService');
    this._user$ = <Subject<IUser>>new Subject();
    this._tasks$ = <Subject<ITask[]>>new Subject();
    this._tags$ = <Subject<ITag[]>>new Subject();
    this._friends$ = <Subject<IFriend[]>>new Subject();
  }

  loadMock(id: string) {
    this.http.get('/assets/data.json')
      .map(response => response.json())
      .subscribe(user => {
        console.log(user);
        this._user$.next(user);
        this._tags$.next(user.tags);
        this._tasks$.next(user.tasks);
        this._friends$.next(user.friends);
      }, error => this.handleError(error));
  }

  load(id: string) {
    this.http.get(`${USER_URL}${id}`)
      .map(response => response.json())
      .subscribe(data => {
        this._user$.next(data[0]);
        this._tags$.next(data[0]['tags']);
        this._tasks$.next(data[0]['tasks']);
        this._friends$.next(data[0]['friends']);
      }, error => this.handleError(error));
  }

  private handleError(error: any) {
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
