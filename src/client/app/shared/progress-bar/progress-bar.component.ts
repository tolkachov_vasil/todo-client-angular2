import { Component, Input, OnInit, OnChanges } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'progress-bar',
    templateUrl: './progress-bar.component.html',
    styleUrls: ['./progress-bar.component.css']
})

export class ProgressBarComponent implements OnInit, OnChanges {
    @Input() value: number;
    private currentValue: number;

    constructor() {
        this.currentValue = 0;
    }

    ngOnInit() { }

    ngOnChanges(changes: any) {
        this.animate(this.value);
    }

    private animate(newValue: number) {
        let delta = (newValue - this.currentValue) / 20;
        let counter = 20;
        let anim = setInterval(() => {
            this.currentValue += delta;
            if (counter-- <= 0) {
                this.currentValue = newValue;
                clearInterval(anim);
            }
        }, 10);
    }


}