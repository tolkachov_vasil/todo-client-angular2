import { Pipe, PipeTransform } from '@angular/core';

import { ITag } from '../models/index';


@Pipe({
    name: 'orderTagBy',
    pure: false
})
export class TagSortPipe implements PipeTransform {
    transform(allTags: ITag[], orders: string[], ignore?: number[]) {
        if (!allTags) return null;
        if (!orders) return allTags;
        orders.forEach(order => {
            switch (order) {
                case 'ignore':
                    allTags = allTags.filter(tag => ignore.indexOf(tag.id) === -1);
                    break;
                case 'use':
                    allTags.sort((tag1, tag2) => tag2.use - tag1.use);
                    break;
                default:
                    break;
            }
        });
        return allTags;
    }
}