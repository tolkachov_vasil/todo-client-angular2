import { Pipe, PipeTransform } from '@angular/core';

import { ITask } from '../models/index';


@Pipe({
    name: 'orderTaskBy',
    pure: false
})
export class TaskSortPipe implements PipeTransform {
    transform(allTasks: ITask[], orders: string[]) {
        if (!allTasks) return null;
        if (!orders) return allTasks;
        orders.forEach(order => {
            switch (order) {
                case 'titleNumber':
                    allTasks.sort((task1: ITask, task2: ITask) => {
                        return parseInt(task1.title, 10) - parseInt(task2.title, 10);
                    });
                    break;
                case 'completed':
                    allTasks.sort((task1: ITask, task2: ITask) => {
                        if (task1.checked && !task2.checked) {
                            return 1;
                        } else if (!task1.checked && task2.checked) {
                            return -1;
                        } else return 0;
                    });
                    break;
                case 'star':
                    allTasks.sort((task1: ITask, task2: ITask) => {
                        if (task1.star && !task2.star) {
                            return -1;
                        } else if (!task1.star && task2.star) {
                            return 1;
                        } else return 0;
                    });
                    break;
                case 'date':
                    allTasks.sort((task1: ITask, task2: ITask) => {
                        if (task1.date > task2.date) {
                            return 1;
                        } else if (task1.date < task2.date) {
                            return -1;
                        } else return 0;
                    });
                    break;
                default:
                    break;
            }
        });
        return allTasks;
    }
}