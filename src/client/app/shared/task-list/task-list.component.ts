import { Component, OnInit, Input } from '@angular/core';
import { ITask, Task } from '../models/index';
import { TodoTaskComponent } from '../todo-task/todo-task.component';
import { TaskSortPipe } from '../pipes/index';


@Component({
    moduleId: module.id,
    selector: 'task-list',
    templateUrl: './task-list.component.html',
    styleUrls: ['./task-list.component.css'],
    directives: [TodoTaskComponent],
    pipes: [TaskSortPipe]
})

export class TaskListComponent implements OnInit {
    @Input() tasks: ITask;
    @Input() hideCompleted: boolean;
    private newTask: ITask;

    constructor() { }

    ngOnInit() {
        this.newTask = new Task();
        this.newTask.title = '';
        this.newTask.child = [];
        this.newTask.tags = [];
        this.newTask._isNewTaskForm = true;
    }

}

