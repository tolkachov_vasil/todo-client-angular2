import { Component, Input } from '@angular/core';

import { Tag } from '../models/index';

@Component({
    moduleId: module.id,
    selector: 'todo-tag',
    templateUrl: './todo-tag.component.html',
    styleUrls: ['./todo-tag.component.css']
})

export class TodoTagComponent {
    @Input() tag: Tag;

}
