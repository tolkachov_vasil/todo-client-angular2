import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'modal-window',
  templateUrl: 'modal-window.component.html',
  styleUrls: ['modal-window.component.css'],
  directives: []
})
export class ModalWindowComponent implements OnInit {
  @Input() show: boolean;
  @Input() multiSelect: boolean;
  @Output() close = new EventEmitter();
  @Output() select = new EventEmitter();

  constructor() { }

  ngOnInit() { }

  onClose(): void {
    this.close.emit(true);
  }

  onSelect(): void {
    this.select.emit(true);
    if (!this.multiSelect) this.close.emit(true);
  }


}
