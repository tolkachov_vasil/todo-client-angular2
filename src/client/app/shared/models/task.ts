export interface ITask {
    id: number;
    title: string;
    checked: boolean;
    inner: boolean;
    child: number[];
    tags: number[];
    unfold: boolean;
    date: Date;
    _progress: number;
    assign: string;
    star: boolean;
    isEditMode: boolean;
    _islocked: boolean;
    _isNewTaskForm: boolean;
}



export class Task implements ITask {
    static isHideCompleted:boolean;
    id: number;
    title: string;
    checked: boolean;
    inner: boolean;
    child: number[];
    tags: number[];
    unfold: boolean;
    date: Date;
    _progress: number;
    assign: string;
    star: boolean;
    isEditMode: boolean;
    _islocked: boolean;
    _isNewTaskForm: boolean;
}
