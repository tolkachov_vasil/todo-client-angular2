import { ITag, ITask, IFriend } from './index';

export interface IUser {
    _id: any;
    tags: ITag[];
    tasks: ITask[];
    name: string;
    image: string;
    friends: IFriend[];
}

export class User implements IUser {

    public get _id(): any { return this._id; }

    public get tags(): ITag[] { return this.tags; }

    public get tasks(): ITask[] { return this.tasks; }

    public get name(): string { return this.name; }

    public get image(): string { return this.image; }

    public get friends(): IFriend[] { return this.friends; }


    constructor(name: string) {
        this.name = name;
    }


}
