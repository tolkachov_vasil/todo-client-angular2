export interface ITag {
    id:number;
    name:string;
    color:string;
    use:number;
    hide:boolean;
}

export class Tag implements ITag {
    id:number;
    name:string;
    color:string;
    use:number;
    hide:boolean;
}