import {
    Component,
    OnInit,
    Input,
    Output,
    EventEmitter
} from '@angular/core';

import { ITag } from '../models/index';
import { TagSortPipe } from '../pipes/index'

@Component({
    moduleId: module.id,
    selector: 'task-filter',
    templateUrl: 'task-filter.component.html',
    styleUrls: ['task-filter.component.css'],
    pipes:[ TagSortPipe ]
})
export class TaskFilterComponent implements OnInit {
    @Input() tags: ITag[];
    @Output() hideCompleted = new EventEmitter();
    private isShowAll: boolean;
    private isHideCompleted: boolean;

    constructor() { }

    ngOnInit() {
        this.isHideCompleted = false;
        this.isShowAll = false;
    }

    toggleAll(): void {
        let val: boolean = !this.isShowAll;
        this.isShowAll = val;
        this.isHideCompleted = val;
        this.tags.forEach(tag => tag.hide = val);
    }

    toggleHideCompleted(): void {
        this.isHideCompleted = !this.isHideCompleted;
        this.hideCompleted.emit(this.isHideCompleted);
    }

    toggleHideByTag(tag: ITag): void {
        tag.hide = !tag.hide;
    }
}
