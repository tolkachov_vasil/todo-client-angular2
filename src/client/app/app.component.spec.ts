import { Component, ComponentResolver, Injector } from '@angular/core';
import { Location } from '@angular/common';
import { disableDeprecatedForms, provideForms } from '@angular/forms/index';
import { TestComponentBuilder } from '@angular/compiler/testing';
import { SpyLocation } from '@angular/common/testing';
import {
  beforeEachProviders,
  async,
  describe,
  expect,
  inject,
  it
} from '@angular/core/testing';

import { AppComponent } from './app.component';

export function main() {

  describe('App component', () => {
    // Disable old forms
    let providerArr: any[];

    it('should build without a problem',
      async(inject([TestComponentBuilder], (tcb:TestComponentBuilder) => {
        tcb.overrideProviders(TestComponent, providerArr)
          .createAsync(TestComponent)
          .then((fixture) => {
            expect(fixture.nativeElement.innerText.indexOf('HOME')).toBeTruthy();
          });
      })));
  });
}

@Component({
  selector: 'test-cmp',
  template: '<sd-app></sd-app>',
  directives: [AppComponent]
})
class TestComponent {
}
