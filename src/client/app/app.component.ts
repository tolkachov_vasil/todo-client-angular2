import { Component, OnInit } from '@angular/core';

import { ITask, ITag, IUser } from './shared/models/index';

import {
  TodoToolbarComponent,
  TaskFilterComponent,
  TaskListComponent } from './shared/index';

import { UserService } from './shared/services/user.service';
import { DataService } from './shared/services/data.service';

let USER_ID = '5759d60e07467d353adc8545';

@Component({
  moduleId: module.id,
  selector: 'todo-app',
  templateUrl: 'app.component.html',
  directives: [TodoToolbarComponent, TaskListComponent, TaskFilterComponent],
})
export class AppComponent implements OnInit {
  private user: IUser;
  private isLogged: boolean;

  constructor(
    private dataService: DataService,
    private userService: UserService) {
  }

  ngOnInit() {
    this.userService.user$.subscribe(user => this.user = user);
    this.doLogin(USER_ID);
  }

  getUsedTags(): ITag[] {
    return this.dataService.getUsedTags();
  }

  getRootTasks(): ITask[] {
    return this.dataService.getRootTasks();
  }


  doLogin(userId: string) {
    this.userService.loadMock(userId);
    this.isLogged = true;
  }

}
